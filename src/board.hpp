#include <vector>

class board{
private:
    int x, y;
    int ** capacities, original_capacitiy;

public:
    board(int x, int y, int** capacities);
    bool isWall(int x, int y);
    void addLight(int x, int y);
}
