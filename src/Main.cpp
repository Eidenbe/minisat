/* Matricule - Nom Prenom
 * (Matricule2 - Nom2 Prenom2)
 * 
 */
#include <iostream>
#include "Solver.hpp"
#include <vector>

std::vector <int> item;
/**
 * Pretty prints the given matrix
 * @param  matrix: matrix to print
 * @param  m: matrix height
 * @param  n: matrix width
 */
void pretty_print(int** matrix, int m, int n) {
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cout << matrix[i][j] << "\t";
    }
    std::cout << std::endl;
  }
}

template<typename T>
bool compare(std::vector<T>& v1, std::vector<T>& v2)
{
    return v1 == v2;
}
void pretty_print_final(int **capacities, int m, int n) {
	for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
            if( capacities[i][j] == -3) std::cout << i << " " << j << std::endl;
        }
    }
    std::cout << std::endl;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            if( capacities[i][j] == -3) std::cout << "X\t";
            else if( capacities[i][j] == -4) std::cout << "-\t";
            else if( capacities[i][j] == 0) std::cout << "0\t";
            else std::cout << capacities[i][j] << "\t";
        
        }
        std::cout << std::endl;
    } 
    std::cout << std::endl;
}


bool checkWall(int ** capacities,int x, int y, int h, int w){
   // std::cout << "check wall func" << std::endl;
    std::vector <int> wall_x;
    std::vector <int> wall_y;
    for(int a=0;a<h;a++)
        if(a-x ==1 || a-x==-1) wall_x.push_back(a);
    
    for(int a=0;a<w;a++)
        if(a-y ==1 || a-y==-1) wall_y.push_back(a);
    for (int i =0; i != wall_x.size(); ++i){
       // std::cout << "Wall_x[] :" << wall_x[i] << std::endl;
       // std::cout << capacities[wall_x[i]][y] << std::endl;
        if(capacities[wall_x[i]][y]==0) return false;
    }
    for (int i =0; i != wall_y.size(); ++i){
       // std::cout << "Wall_y[] :" << wall_y[i] << std::endl;
        //std::cout << capacities[x][wall_y[i]] << std::endl;

        if(capacities[x][wall_y[i]]==0) return false;
    }
    for (int i =0; i != wall_x.size(); ++i){
        if(capacities[wall_x[i]][y]>0) capacities[wall_x[i]][y] = capacities[wall_x[i]][y] -1;
    }
    for (int i =0; i != wall_y.size(); ++i){
        if(capacities[x][wall_y[i]]>0) capacities[x][wall_y[i]] = capacities[x][wall_y[i]] -1;
    }
    return true;
}

bool isWall(int ** capacities,int x, int y){
    return capacities[x][y]>=-1;
}

void addLight(int ** capacities,int x, int y,int h, int w){
    if(checkWall(capacities,x,y,h,w)){
        capacities[x][y]=-3;
        int old_y =y;
        int old_x=x;
        for(y=old_y+1;y<h && isWall(capacities,x,y)!=true;++y){
            capacities[x][y]=-4;
        }
        for(y=old_y-1;y>=0 && isWall(capacities,x,y)!=true ;--y){
            capacities[x][y]=-4;        
        }
        y=old_y;
        for(x=old_x+1;x<w && isWall(capacities,x,y)!=true;++x){
            capacities[x][y]=-4;
        }
        for(x=old_x-1;x>=0 && isWall(capacities,x,y)!=true;--x){
            capacities[x][y]=-4;
        }
    }
}
bool already_have(int ** capacities, int h, int w){
    //push a content of a vector into a vector? 
    if(item.empty()){
        //std::cout << "empty" << std::endl;
        for(int x=0;x<h;x++){
            for(int y=0; y<w;y++){
                item.push_back(capacities[x][y]);
            }
        }
    }else{
        std::vector <int> vec_to_test;
        std::vector <int> vec2;
        for(int x=0;x<h;x++){
            for(int y=0; y<w;y++){
                vec_to_test.push_back(capacities[x][y]);
            }
        }
        int test=0;
        for(int x=0;x<item.size();x++){ //I think we can simplify it with only using two vectors instead of """3"""
            vec2.push_back(item[x]);
            test++;
            if((test%((w*h)))==0 && x!=0){
                if(compare(vec2,vec_to_test)) {
                    return true;
                } 
                vec2.clear();
                test=0;
            }
        }
         for(int x=0;x<h;x++){
            for(int y=0; y<w;y++){
                item.push_back(capacities[x][y]);
            }
        }
        vec_to_test.clear();
    }
     return false;
}
bool check(int ** capacities,int h, int w){
    for(int x=0;x<w;x++){
        for(int y=0; y<h;y++){
            if(capacities[x][y]==-2 || capacities[x][y]>0) return true;
        }
    }
    return already_have(capacities,h,w);
}


/**
 * Solves the given light-up instance.
 * @param capacities: instance capacities to solve, an `m` by `n` matrix.
 * @param m: height of the instance
 * @param n: width of the instance
 * @param find_all: if true, find all valid solutions
 */  
void solve(int** array, int h, int w, bool find_all) {
	// Fonction à compléter pour les questions 2 et 3 (et bonus 1)
	int ** cap = new int *[h];
	int ** generated = new int *[h];
	int ** capacities = new int*[h];
	int ** walls = new int*[h];
	for(int x=0;x<h;x++){
		capacities[x] = new int[w];
		generated[x] = new int[w];
		cap[x] = new int[w];
		walls[x] = new int[w];
		for(int y=0;y<w;y++){
			capacities[x][y]=array[x][y];
			cap[x][y]=array[x][y];
			generated[x][y]=array[x][y];
            walls[x][y]=array[x][y];
		}
	}
    //std::cout << "Check wall 1 1 " << checkWall(capacities,1,1,h,w) <<std::endl;
    pretty_print_final(capacities,h,w);
    bool found_all=false;
    int old_x=0;
    int old_y=0;
    int tries=0;
    int g=0;
    int current=0;
    std::cout << "Number of Tries to do: " << w*h << std::endl;
    while(found_all!=true){
        for(int x=0;x<h;x++){
            for(int y=0;y<w;y++){
                if(capacities[x][y]==-2 && capacities[x][y] <0 && current==tries){
                     addLight(capacities,x,y,h,w);
                }
                current++;
            }
        }
        for (int x=0;x<h;x++){
            for(int y=0;y<w;y++){
                generated[x][y]=capacities[x][y];
            }
        }
        bool restart = true;
        current=0;
        for(int z=0;z<(w*h);z++){
            for(int x=0;x<h;x++){
                for(int y=0;y<w;y++){
                    if(capacities[x][y]==-2 && capacities[x][y] <0 &&g>=z){
                        addLight(capacities,x,y,h,w);

                        if(g>=z && restart){
                            restart = false;
                            y=0;
                            x=0;
                        }
                    }
                    g++;
                }
            }
            restart = true;
            g=0;
            pretty_print_final(capacities,h,w);

            if (!check(capacities,w,h)) {
                std::cout << "Solution :" << std::endl;
                for(int x=0;x<h;x++){
		        for(int y=0;y<w;y++){
                        if(walls[x][y]>0) capacities[x][y] = walls[x][y];
	        	    }
	            }
                pretty_print_final(capacities,h,w);
            }
            for (int x=0;x<h;x++){
                for(int y=0;y<w;y++){
                    capacities[x][y]=generated[x][y];
                }
            }

        }
        for (int x=0;x<w;x++){
            for(int y=0;y<h;y++){
                capacities[x][y]=cap[x][y];
            }
        }
        if(tries==w*h) found_all=true;
        std::cout << "Tries : " << tries++ << std::endl;
    }
}
/**
 * Generates `l` instances of the light-up problem, each with a unique solution,
 * and prints them on the standard output.
 * @param instance: instance to solve, an `m` by `n` matrix.
 * @param m: height of each instance
 * @param n: width of each instance
 */
void generate(int m, int n, int l) {
  // Fonction à compléter pour la question bonus 2

}

/**
 * Prints program help message
 * @param program: program name
 */
void print_help(char* program) {
  std::cerr << "Usage:" << std::endl;
  std::cerr << program << " [-g HAUTEUR LARGEUR NOMBRE]" << std::endl;
}

int main(int argc, char** argv) {
  int m, n, l;
  bool gen = false;
  bool find_all = false;

  // Parse command line arguments
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-g") == 0) {
      gen = true;
      try {
        m = std::stoi(argv[i + 1]);
        n = std::stoi(argv[i + 2]);
        l = std::stoi(argv[i + 3]);
      } catch (const std::logic_error& e) {
        print_help(argv[0]);
        return EXIT_FAILURE;
      }
    } else if (strcmp(argv[i], "-a") == 0) {
      find_all=true;
     } else if ((strcmp(argv[i], "--help") == 0) ||
               (strcmp(argv[i], "-h") == 0)) {
      print_help(argv[0]);
      return EXIT_SUCCESS;
    }
  }

  if (gen) {
    generate(m, n, l);
  } else {
    // read instance on standard input
    std::cin >> m >> n;
    int** capacities = new int*[m];
    for (int i = 0; i < m; ++i) {
      capacities[i] = new int[n];
      for (int j = 0; j < n; ++j) {
        std::cin >> capacities[i][j];
      }
    }

    solve(capacities, m, n, find_all);
  }

  return EXIT_SUCCESS;
}
