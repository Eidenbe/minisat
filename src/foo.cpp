#include <iostream>
#include <vector>
#include <cstdlib>
std::vector <int> item;

class matrix{
public:
    int rows;
    int cols;
    int * data;
    int &operator[](int i) {
    //return data + (i*cols);
    return rows;
}
    matrix(){
    this->data = new int [4];
    this->rows = 2;
    this->cols = 2;
    data[0] = 0;
    data[1] = 1;
    data[2] = 2;
    data[3] = 3;
    }
};

int capacities[4][4] = {
       {-2,-2,0,-2},
       {0,-2,-2,0},
       {-2,-2,-2,-2},
       {-2,0,-2,-2},
};
int cap[4][4] = {
       {-2,-2,0,-2},
       {0,-2,-2,0},
       {-2,-2,-2,-2},
       {-2,0,-2,-2},
};
int generated[4][4] = {
       {-2,-2,0,-2},
       {0,-2,-2,0},
       {-2,-2,-2,-2},
       {-2,0,-2,-2},
};

template<typename T>
bool compare(std::vector<T>& v1, std::vector<T>& v2)
{
    return v1 == v2;
}

void pretty_print( int m, int n) {
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cout << capacities[i][j] << "\t";
    }
    std::cout << std::endl;
  }
}

void pretty_print_final( int m, int n) {
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
        if( capacities[i][j] == -3) std::cout << "X\t";
        else if( capacities[i][j] == -4) std::cout << "-\t";
        else if( capacities[i][j] == 0) std::cout << "0\t";
        else std::cout << capacities[i][j] << "\t";
        
    }
    std::cout << std::endl;
  }
    std::cout << std::endl;

}

bool isWall(int x, int y){
    return capacities[x][y]>=0;
}

void addLight(int x, int y){
    capacities[x][y]=-3;
    int old_y =y;
    int old_x=x;
    for(y=old_y+1;isWall(x,y)!=true && y<=3;++y){
        capacities[x][y]=-4;
    }
    for(y=old_y-1;isWall(x,y)!=true && y>=0;--y){
        capacities[x][y]=-4;        
    }
    y=old_y;
    for(x=old_x+1;isWall(x,y)!=true && x<=3;++x){
        capacities[x][y]=-4;
    }
    for(x=old_x-1;isWall(x,y)!=true && x>=0;--x){
        capacities[x][y]=-4;
    }
}
bool already_have(int w, int h){
    //push a content of a vector into a vector? 
    if(item.empty()){
        //std::cout << "empty" << std::endl;
        for(int x=0;x<w;x++){
            for(int y=0; y<h;y++){
                item.push_back(capacities[x][y]);
            }
        }
    }else{
        std::vector <int> vec_to_test;
        std::vector <int> vec2;
        for(int x=0;x<w;x++){
            for(int y=0; y<h;y++){
                vec_to_test.push_back(capacities[x][y]);
            }
        }
        int test=0;
        for(int x=0;x<item.size();x++){ //I think we can simplify it with only using two vectors instead of """3"""
            vec2.push_back(item[x]);
            test++;
            if((test%((w*h)))==0 && x!=0){
                // std::cout << "on est dans le modulo" << std::endl;
                // std::cout << "vec2: " << std::endl;

                // for (std::vector<int>::iterator it = vec2.begin() ; it != vec2.end(); ++it){
                //     std::cout << ' ' << *it;
                // }
                // std::cout << "\nvec to test: " << std::endl;
                // for (std::vector<int>::iterator it = vec_to_test.begin() ; it != vec_to_test.end(); ++it){
                //     std::cout << ' ' << *it;
                // }
                // std::cout << "\n" << std::endl;
                if(compare(vec2,vec_to_test)) {
                    //std::cout << "on retourne vrai" << std::endl;
                    return true;
                } 
                vec2.clear();
                test=0;
            }
        }
         for(int x=0;x<w;x++){
            for(int y=0; y<h;y++){
                item.push_back(capacities[x][y]);
            }
        }
        vec_to_test.clear();
    }
     return false;
}
bool check(int w, int h){
    for(int x=0;x<w;x++){
        for(int y=0; y<h;y++){
            if(capacities[x][y]==-2) return true; 
        }
    }
    return already_have(w,h);
}

void solve(int ** capacities, int w, int h){
    //check now if the solution generated hasn't been generated before
    bool found_all=false;
    int old_x=0;
    int old_y=0;
    int tries=0;
    int g=0;
    int current=0;
    while(found_all!=true){
        for(int x=0;x<w;x++){
            for(int y=0;y<h;y++){
                if(capacities[x][y]==-2 && capacities[x][y] <0 && current==tries){
                    addLight(x,y);
                }
                current++;
            }
        }
        for (int x=0;x<w;x++){
            for(int y=0;y<h;y++){
                generated[x][y]=capacities[x][y];
            }
        }
        //std::cout << "Tries : " << tries << std::endl;
        //std::cout << "Gen:\n"; pretty_print_final(w,h);
        bool restart = true;
        current=0;
        for(int z=0;z<(w*h);z++){
            for(int x=0;x<w;x++){
                for(int y=0;y<h;y++){
                    //std::cout << "G value : " << g <<std::endl;
                    if(capacities[x][y]==-2 && capacities[x][y] <0 &&g>=z){
                        //std::cout << "Adding a light on: " << "x: " << x << " y: " << y << std::endl;    
                        addLight(x,y);
                        //pretty_print_final(4,4); 
                        if(g>=z && restart){
                            restart = false;
                            y=0;
                            x=0;
                        }
                    }
                    g++;
                }
            }
            restart = true;
            g=0;
            if (!check(w,h)) {
                std::cout << "Solution :" << std::endl;
                pretty_print_final(w,h);
            }
            for (int x=0;x<w;x++){
                for(int y=0;y<h;y++){
                    capacities[x][y]=generated[x][y];
                }
            }

        }
        for (int x=0;x<w;x++){
            for(int y=0;y<h;y++){
                capacities[x][y]=cap[x][y];
            }
        }
        if(tries==w*h) found_all=true;
        tries++;
    }
}

template <size_t rows, size_t cols>
void process_2d_array_template(int (&array)[rows][cols])
{
    std::cout << __func__ << std::endl;
    for (size_t i = 0; i < rows; ++i)
    {
        std::cout << i << ": ";
        for (size_t j = 0; j < cols; ++j)
            std::cout << array[i][j] << '\t';
        std::cout << std::endl;
    }
}

int main(){
    pretty_print(4,4);

   //addLight(1,2);
    std::cout << std::endl;
    //solve(capacities,4,4);
    int test[4][4] = {
       {-2,-2,0,-2},
       {0,-2,-2,0},
       {-2,-2,-2,-2},
       {-2,0,-2,-2},
};
    //process_2d_array_template(test);
    return 0;
}
