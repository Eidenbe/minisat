/* Matricule - Nom Prenom
 * (Matricule2 - Nom2 Prenom2)
 * 
 */
#include <iostream>
#include "Solver.hpp"
#include <vector>
#include <cstdlib>

#define FOR(k,a,b) for (int k = (a); (k) < (b); (k)++)
#define FORMINUS(k,a,b) for (int k = (a); (k) >= (b); (k)--)

bool isSolvable=false;

/**
 * Pretty prints the given matrix
 * @param  matrix: matrix to print
 * @param  m: matrix height
 * @param  n: matrix width
 */
void pretty_print(int** matrix, int m, int n) {
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cout << matrix[i][j] << "\t";
    }
    std::cout << std::endl;
  }
}
void pretty_print_final(int **capacities, int m, int n) {
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            if( capacities[i][j] == -3) std::cout << "X\t";
            else if( capacities[i][j] == -4 || capacities[i][j] == -2) std::cout << "-\t";
            else if( capacities[i][j] == 0) std::cout << "0\t";
            else std::cout << capacities[i][j] << "\t";
        
        }
        std::cout << std::endl;
    } 
    std::cout << std::endl;
}
bool checkWall(int ** capacities,int x, int y, int h, int w){
   // std::cout << "check wall func" << std::endl;
    std::vector <int> wall_x;
    std::vector <int> wall_y;
    for(int a=0;a<h;a++)
        if(a-x ==1 || a-x==-1) wall_x.push_back(a);
    
    for(int a=0;a<w;a++)
        if(a-y ==1 || a-y==-1) wall_y.push_back(a);
    for (uint i =0; i != wall_x.size(); ++i){
       // std::cout << "Wall_x[] :" << wall_x[i] << std::endl;
       // std::cout << capacities[wall_x[i]][y] << std::endl;
        if(capacities[wall_x[i]][y]==0) return false;
    }
    for (uint i =0; i != wall_y.size(); ++i){
       // std::cout << "Wall_y[] :" << wall_y[i] << std::endl;
        //std::cout << capacities[x][wall_y[i]] << std::endl;

        if(capacities[x][wall_y[i]]==0) return false;
    }
    for (uint i =0; i != wall_x.size(); ++i){
        if(capacities[wall_x[i]][y]>0) capacities[wall_x[i]][y] = capacities[wall_x[i]][y] -1;
    }
    for (uint i =0; i != wall_y.size(); ++i){
        if(capacities[x][wall_y[i]]>0) capacities[x][wall_y[i]] = capacities[x][wall_y[i]] -1;
    }
    return true;
}
bool isWall(int ** capacities,int x, int y){
    return capacities[x][y]>=-1;
}

void addLight(int ** capacities,int x, int y,int h, int w){
    //if(checkWall(capacities,x,y,h,w)){
        capacities[x][y]=-3;
        int old_y =y;
        int old_x=x;
        for(y=old_y+1;y<w && capacities[x][y]!=-3 && isWall(capacities,x,y)!=true;++y){
            capacities[x][y]=-4;
        }
        for(y=old_y-1;y>0  && capacities[x][y]!=-3 && isWall(capacities,x,y)!=true ;--y){
            capacities[x][y]=-4;
        }
        y=old_y;
        for(x=old_x+1;x<h  && capacities[x][y]!=-3 && isWall(capacities,x,y)!=true;++x){
            capacities[x][y]=-4;
        }

        for(x=old_x-1;x>0  && capacities[x][y]!=-3 && isWall(capacities,x,y)!=true;--x){
            capacities[x][y]=-4;
        }

   // }
}
/**
 * Solves the given light-up instance.
 * @param capacities: instance capacities to solve, an `m` by `n` matrix.
 * @param m: height of the instance
 * @param n: width of the instance
 * @param find_all: if true, find all valid solutions
 */  
void solve(int** capacities, int h, int w, bool find_all) {
  // Fonction à compléter pour les questions 2 et 3 (et bonus 1)

	Solver s;
	vec <Lit> lits;
	vec <Lit> lits2;
    int prop[h][w];
	int ** generated = new int *[h];    
	for(int x=0;x<h;x++){
		generated[x] = new int[w];		
		for(int y=0;y<w;y++){
			generated[x][y]=capacities[x][y];
		}
	}
    FOR(r, 0, h) {
      FOR(c, 0, w) {
	    prop[r][c] = s.newVar();
        }
    }
   lits.clear();
   //Je peux mettre une ampoule sur chaque case -2
    FOR(i,0,h){
        FOR(j,0,w){
            if(capacities[i][j]==-2) lits.push(Lit(prop[i][j]));
            else s.addUnit(~Lit(prop[i][j]));
        }
    }
    s.addClause(lits);
    //Si je mets une ampoule sur une ligne ou une colonne, je ne veux pas qu'il y en ait d'autre sur celle si (je m'arrête à un mur)
    FOR(i,0,h) {
        FOR(j,0,w) {
            if(capacities[i][j]==-2){
                FOR(x,i+1,h) {
                    if (capacities[x][j]==-2) s.addBinary(~Lit(prop[i][j]),~Lit(prop[x][j]));
                    else break;
                }
                FORMINUS(x,i-1,0) {
                    if (capacities[x][j]==-2) s.addBinary(~Lit(prop[i][j]),~Lit(prop[x][j]));
                    else break;
                }
                FOR(y,j+1,w) {
                    if (capacities[i][y]==-2) s.addBinary(~Lit(prop[i][j]),~Lit(prop[i][y]));
                    else break;
                }
                FORMINUS(y,j-1,0) {
                    if (capacities[i][y]==-2) s.addBinary(~Lit(prop[i][j]),~Lit(prop[i][y]));
                    else break;
                }
            }
        }
    }
    //Je dis que chaque case doit avoir une lampe sur sa ligne/colonne
    FOR(i,0,h){
        FOR(j,0,w){
            if(capacities[i][j]==-2){
                lits.clear();
                lits.push(Lit(prop[i][j]));
                FOR(x,i+1,h){
                    if(capacities[x][j]==-2) lits.push(Lit(prop[x][j]));
                    else break;
                }
                FORMINUS(x,i-1,0){
                    if(capacities[x][j]==-2) lits.push(Lit(prop[x][j]));
                    else break;
                }   
                FOR(y,j+1,w){
                    if(capacities[i][y]==-2) lits.push(Lit(prop[i][y]));
                    else break;
                }
                FORMINUS(y,j-1,0){
                    if(capacities[i][y]==-2) lits.push(Lit(prop[i][y]));
                    else break;
                }
                s.addClause(lits);        
            }
        }
    }
    //Mes clauses de mur, je teste pour chaque mur de capacité, si il est soit: dans un coin, entre deux coins, ou dans le tableau entier.
    FOR(x,0,h){
		FOR(y,0,w){
            //std:: cout << "x y :" << x << " " << y << std::endl;
            //std::cout << "capacities[" << x << "][" <<y << "] = " << capacities[x][y] << std::endl;
            if(capacities[x][y]==0){
                //CLause pour le mur 0, on dit simplement qu'on ne doit pas mettre d'ampoules à proximités
                if(x==0 && y==0) {s.addUnit(~Lit(prop[x+1][y])); s.addUnit(~Lit(prop[x][y+1]));}
                else if(x<h-1 && y==0) {s.addUnit(~Lit(prop[x+1][y])); s.addUnit(~Lit(prop[x-1][y])); s.addUnit(~Lit(prop[x][y+1]));}
                else if(x==h-1 && y==0) {s.addUnit(~Lit(prop[x-1][y])); s.addUnit(~Lit(prop[x][y+1]));}

                else if(x==0 && y<w-1) {s.addUnit(~Lit(prop[x+1][y]));s.addUnit(~Lit(prop[x][y+1])); s.addUnit(~Lit(prop[x][y-1]));}
                else if(x<h-1 && y<w-1) {s.addUnit(~Lit(prop[x+1][y]));s.addUnit(~Lit(prop[x-1][y]));s.addUnit(~Lit(prop[x][y+1]));s.addUnit(~Lit(prop[x][y-1]));}
                else if(x==h-1 && y<w-1) {s.addUnit(~Lit(prop[x-1][y]));s.addUnit(~Lit(prop[x][y+1]));s.addUnit(~Lit(prop[x][y-1]));}

                else if(x==0 && y==w-1) {s.addUnit(~Lit(prop[x+1][y]));s.addUnit(~Lit(prop[x][y-1]));}
                else if(x<h-1 && y==w-1) { s.addUnit(~Lit(prop[x+1][y]));s.addUnit(~Lit(prop[x-1][y]));s.addUnit(~Lit(prop[x][y-1]));}
                else if(x==h-1 && y==w-1) {s.addUnit(~Lit(prop[x-1][y]));s.addUnit(~Lit(prop[x][y-1]));}
            }
            if(capacities[x][y]==1){
                //Clause pour le mur 1, on check la position du mur et on avise (si il est en coin, au milieu ou entre deux coins)
                //Globalement je dis qu'il peut mettre une ampoule sur chaque case à proximité, et je dis qu'il ne doit pas en mettre plus d'une.
                if(x==0 && y==0){
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    s.addClause(lits);
                    s.addBinary(~Lit(prop[x+1][y]),~Lit(prop[x][y+1]));
                }
                else if(x<h-1 && y==0){
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    s.addClause(lits);
                    s.addTernary(~Lit(prop[x+1][y]),~Lit(prop[x][y+1]),~Lit(prop[x-1][y]));
                }
                else if(x==h-1 && y==0) {
                    lits.clear();
                    if (capacities[x-1][y]==-2)  lits.push(Lit(prop[x-1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    s.addClause(lits);
                    s.addBinary(~Lit(prop[x-1][y]),~Lit(prop[x][y+1]));
                }

                else if(x==0 && y<w-1) {
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    s.addClause(lits);
                    s.addTernary(~Lit(prop[x+1][y]),~Lit(prop[x][y+1]),~Lit(prop[x][y-1]));
                }
                else if(x<h-1 && y<w-1)  {
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    s.addClause(lits);
                    s.addBinary(~Lit(prop[x-1][y]),~Lit(prop[x][y+1]));
                    s.addBinary(~Lit(prop[x-1][y]),~Lit(prop[x+1][y]));
                    s.addBinary(~Lit(prop[x-1][y]),~Lit(prop[x][y-1]));
                    s.addBinary(~Lit(prop[x+1][y]),~Lit(prop[x][y-1]));
                    s.addBinary(~Lit(prop[x+1][y]),~Lit(prop[x][y+1]));
                    s.addBinary(~Lit(prop[x][y+1]),~Lit(prop[x][y-1]));                    
                }
                else if(x==h-1 && y<w-1)  {
                    lits.clear();
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    s.addClause(lits);
                    s.addTernary(~Lit(prop[x-1][y]),~Lit(prop[x][y+1]),~Lit(prop[x][y-1]));
                }

                else if(x==0 && y==w-1) {
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    s.addClause(lits);
                    s.addBinary(~Lit(prop[x+1][y]),~Lit(prop[x][y-1]));
                } 
                else if(x<h-1 && y==w-1) {
                    lits.clear();
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    s.addClause(lits);
                    s.addTernary(~Lit(prop[x-1][y]),~Lit(prop[x+1][y]),~Lit(prop[x][y-1]));
                } 
                else if(x==h-1 && y==w-1)  {
                    lits.clear();
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    s.addClause(lits);
                    s.addBinary(~Lit(prop[x-1][y]),~Lit(prop[x][y-1]));
                }
            }
            if(capacities[x][y]==2){
                //Clause pour le mur 2, on check la position du mur et on avise (si il est en coin, au milieu ou entre deux coins)
                //Globalement je dis qu'il peut mettre une ampoule sur chaque case à proximité, et je dis qu'il ne doit pas en mettre plus de deux
                if(x==0 && y==0){
                    lits.clear();
                    if(capacities[x+1][y]==-2 &&capacities[x][y+1]==-2){
                        s.addUnit(Lit(prop[x+1][y]));
                        s.addUnit(Lit(prop[x][y+1]));                        
                    }else{
                        s.addClause(lits);
                    }
                }
                else if(x<h-1 && y==0){
                    lits.clear();
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    s.addClause(lits);
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x-1][y]));
                    s.addTernary(Lit(prop[x-1][y]),Lit(prop[x][y+1]),Lit(prop[x+1][y]));
                    s.addTernary(~Lit(prop[x-1][y]),~Lit(prop[x][y+1]),~Lit(prop[x+1][y]));
                   
                }
                else if(x==h-1 && y==0) {
                    lits.clear();
                    if(capacities[x-1][y]==-2 &&capacities[x][y+1]==-2){
                        s.addUnit(Lit(prop[x-1][y]));
                        s.addUnit(Lit(prop[x][y+1]));                        
                    }else{
                        s.addClause(lits);
                    }
                }

                else if(x==0 && y<w-1) {
                    lits.clear();
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    s.addClause(lits);
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x][y-1]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x][y-1]));
                    s.addTernary(Lit(prop[x][y+1]),Lit(prop[x][y+1]),Lit(prop[x+1][y]));
                    s.addTernary(~Lit(prop[x][y+1]),~Lit(prop[x][y+1]),~Lit(prop[x+1][y]));
                }
                else if(x<h-1 && y<w-1)  {
                    
                    s.addTernary(Lit(prop[x-1][y]),Lit(prop[x][y+1]),Lit(prop[x+1][y]));
                    s.addTernary(Lit(prop[x][y-1]),Lit(prop[x][y+1]),Lit(prop[x+1][y]));
                    s.addTernary(Lit(prop[x][y-1]),Lit(prop[x][y+1]),Lit(prop[x-1][y]));
                   
                    s.addTernary(~Lit(prop[x-1][y]),~Lit(prop[x][y+1]),~Lit(prop[x+1][y]));
                    s.addTernary(~Lit(prop[x][y-1]),~Lit(prop[x][y+1]),~Lit(prop[x+1][y]));
                    s.addTernary(~Lit(prop[x][y-1]),~Lit(prop[x][y+1]),~Lit(prop[x-1][y]));

                }
                else if(x==h-1 && y<w-1)  {
                    lits.clear();
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    if (capacities[x][y+1]==-2) lits.push(Lit(prop[x][y+1]));
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    s.addClause(lits);
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x][y-1]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y-1]));
                    s.addTernary(Lit(prop[x][y+1]),Lit(prop[x][y+1]),Lit(prop[x-1][y]));
                    s.addTernary(~Lit(prop[x][y+1]),~Lit(prop[x][y+1]),~Lit(prop[x-1][y]));
                }

                else if(x==0 && y==w-1) {
                    lits.clear();
                    lits.clear();
                    if(capacities[x+1][y]==-2 &&capacities[x][y-1]==-2){
                        s.addUnit(Lit(prop[x+1][y]));
                        s.addUnit(Lit(prop[x][y-1]));                        
                    }else{
                        s.addClause(lits);
                    }
                } 
                else if(x<h-1 && y==w-1) {
                    lits.clear();
                    if (capacities[x][y-1]==-2) lits.push(Lit(prop[x][y-1]));
                    if (capacities[x+1][y]==-2) lits.push(Lit(prop[x+1][y]));
                    if (capacities[x-1][y]==-2) lits.push(Lit(prop[x-1][y]));
                    s.addClause(lits);
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x+1][y]));
                    s.addBinary(Lit(prop[x][y-1]),Lit(prop[x+1][y]));
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y-1]));
                    s.addTernary(Lit(prop[x+1][y]),Lit(prop[x][y-1]),Lit(prop[x-1][y]));
                    s.addTernary(~Lit(prop[x][y-1]),~Lit(prop[x+1][y]),~Lit(prop[x-1][y]));
                } 
                else if(x==h-1 && y==w-1)  {
                    lits.clear();
                    lits.clear();
                    if(capacities[x-1][y]==-2 &&capacities[x][y-1]==-2){
                        s.addUnit(Lit(prop[x-1][y]));
                        s.addUnit(Lit(prop[x][y-1]));                        
                    }else{
                        s.addClause(lits);
                    }
                }
            }
            if(capacities[x][y]==3){
                //Clause pour le mur 3, on check la position du mur et on avise (si il est en coin, au milieu ou entre deux coins)
                //Globalement je dis qu'il peut mettre une ampoule sur chaque case à proximité, et je dis qu'il ne doit pas en mettre plus de trois
                //Ici c'est déjà plus simple, si le mur est dans un coin c'est impossible d'en mettre trois, et si il est entre deux coins il force à en mettre sur
                //chacune de ses cases à proximités
                if(x==0 && y==0){
                    lits.clear();
                     s.addClause(lits);
                }
                else if(x<h-1 && y==0){
                    lits.clear();
                    if(capacities[x-1][y]==-2 &&capacities[x][y+1]==-2 && capacities[x-1][y]==-2){
                        s.addUnit(Lit(prop[x-1][y]));
                        s.addUnit(Lit(prop[x][y+1]));     
                        s.addUnit(Lit(prop[x+1][y]));                        
                    }else{
                        s.addClause(lits);
                    }
                   
                }
                else if(x==h-1 && y==0) {
                    lits.clear();
                     s.addClause(lits);
                    
                }

                else if(x==0 && y<w-1) {
                    lits.clear();
                    if(capacities[x+1][y]==-2 &&capacities[x][y+1]==-2 && capacities[x][y-1]==-2){
                        s.addUnit(Lit(prop[x+1][y]));
                        s.addUnit(Lit(prop[x][y+1]));     
                        s.addUnit(Lit(prop[x][y-1]));                        
                    }else{
                        s.addClause(lits);
                    }
                }
                else if(x<h-1 && y<w-1)  {
                    lits.clear();
                    if (capacities[x+1][y]==-2) lits.push(~Lit(prop[x+1][y]));
                    if (capacities[x][y+1]==-2) lits.push(~Lit(prop[x][y+1]));
                    if (capacities[x][y-1]==-2) lits.push(~Lit(prop[x][y-1]));
                    if (capacities[x-1][y]==-2) lits.push(~Lit(prop[x-1][y]));
                    s.addClause(lits);
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x+1][y]));
                    s.addBinary(Lit(prop[x-1][y]),Lit(prop[x][y-1]));
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x][y-1]));
                    s.addBinary(Lit(prop[x+1][y]),Lit(prop[x][y+1]));
                    s.addBinary(Lit(prop[x][y+1]),Lit(prop[x][y-1]));            
                }
                else if(x==h-1 && y<w-1)  {
                    lits.clear();
                    if(capacities[x-1][y]==-2 &&capacities[x][y+1]==-2 && capacities[x][y-1]==-2){
                        s.addUnit(Lit(prop[x-1][y]));
                        s.addUnit(Lit(prop[x][y+1]));     
                        s.addUnit(Lit(prop[x][y-1]));                        
                    }else{
                        s.addClause(lits);
                    }
                }

                else if(x==0 && y==w-1) {
                    lits.clear();
                     s.addClause(lits);
                    
                } 
                else if(x<h-1 && y==w-1) {
                    lits.clear();
                    if(capacities[x+1][y]==-2 &&capacities[x][y-1]==-2 && capacities[x-1][y]==-2){
                        s.addUnit(Lit(prop[x+1][y]));
                        s.addUnit(Lit(prop[x][y-1]));     
                        s.addUnit(Lit(prop[x-1][y]));                        
                    }else{
                        s.addClause(lits);
                    }
                   
                } 
                else if(x==h-1 && y==w-1)  {
                    lits.clear();
                    s.addClause(lits);                    
                }
            }
            if(capacities[x][y]==4){
                //Clause pour le mur 4, on check la position du mur et on avise (si il est en coin, au milieu ou entre deux coins)
                //Le summum de la facilité, pour qu'un mur 4 soit satisfaisable, il doit obligatoirement être au milieu, sinon c'est impossible.
                lits.clear();
                if(x<h-1 && y<w-1){
                    if(capacities[x+1][y]==-2 && capacities[x][y-1]==-2 && capacities[x-1][y]==-2 && capacities[x][y+1]==-2){
                        s.addUnit(Lit(prop[x+1][y]));
                        s.addUnit(Lit(prop[x][y-1]));     
                        s.addUnit(Lit(prop[x-1][y]));  
                        s.addUnit(Lit(prop[x][y+1]));                        
                    }else{
                        s.addClause(lits);
                    }
                    
                }else  s.addClause(lits);
            }
		}
    }
    //Je solve et trouve toutes les solutions possibles si find_all = true
    do{
        s.solve();
        if(s.okay()){
            lits.clear();
  	        pretty_print(capacities, h, w);//J'affiche l'input originel
            //J'affiche les lampes en parcourant le tableau
            FOR(i,0,h) {
                FOR(j,0,w) {
                    if (s.model[prop[i][j]] == l_True) {
                        std::cout<< i << " " << j << "\n";
                        addLight(capacities,i,j,h,w);
                        lits.push(~Lit(prop[i][j]));//Je dis que la positions des lampes n'est pas bonne, pour qu'il soit forcé de changer leur positions
                    }
                }
            }
                std::cout << "\n" ;
            s.addClause(lits);
            isSolvable=true;
            //pretty_print(capacities,h,w);
            pretty_print_final(capacities,h,w);
            //Comme je modifie le tableau quand j'ajoute une lampe je veux qu'il revienne à la normale
            capacities = new int *[h];    
	        for(int x=0;x<h;x++){
		        capacities[x] = new int[w];		
	        	for(int y=0;y<w;y++){
		        	capacities[x][y]=generated[x][y];
	        	}
	        }

        }else{
            find_all=false; //Si il ne trouve pas de solution j'arrête de chercher des solutions.
            std::cout << "Insolvable\n"; 
              
        }
    }while(find_all==true);
}

int fill_randomly(){
    int n;
    n = rand() % 10 +1; //Random number between 1 and 10
    if(n<=6) return -2; //If the random is between 1 and 7, it'll be a empty case
    n = rand() % 20 +1; //If not, it will be a wall
    if(n<=5) return -1;
    if(n<=7) return 0;
    if(n<=15) return 1;
    if(n<=16) return 2;
    if(n<=18) return 3;
    //if(n<=19) return 4;
    return -2;
}
/**
 * Generates `l` instances of the light-up problem, each with a unique solution,
 * and prints them on the standard output.
 * @param instance: instance to solve, an `m` by `n` matrix.
 * @param h: height of each instance
 * @param w: width of each instance
 */
void generate(int h, int w, int l) {
    srand (time(0));
  // Fonction à compléter pour la question bonus 2
    int count=0;
    //int i=0;
    while(count<l){
        int ** capacities = new int *[h];
        for(int i=0;i<h;i++){
            capacities[i] = new int [w];
            for(int j=0;j<w;j++){
                capacities[i][j] = fill_randomly();
            }
        }
        //std::cout << i++ << std::endl;
        //pretty_print(capacities,h,w);
        solve(capacities,h,w,false);
        if(isSolvable){ isSolvable=false; count++;}
    }
}

/**
 * Prints program help message
 * @param program: program name
 */
void print_help(char* program) {
  std::cerr << "Usage:" << std::endl;
  std::cerr << program << " [-g HAUTEUR LARGEUR NOMBRE]" << std::endl;
}

int main(int argc, char** argv) {
  int m, n, l;
  bool gen = false;
  bool find_all = false;

  // Parse command line arguments
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-g") == 0) {
      gen = true;
      try {
        m = std::stoi(argv[i + 1]);
        n = std::stoi(argv[i + 2]);
        l = std::stoi(argv[i + 3]);
      } catch (const std::logic_error& e) {
        print_help(argv[0]);
        return EXIT_FAILURE;
      }
    } else if (strcmp(argv[i], "-a") == 0) {
      find_all=true;
     } else if ((strcmp(argv[i], "--help") == 0) ||
               (strcmp(argv[i], "-h") == 0)) {
      print_help(argv[0]);
      return EXIT_SUCCESS;
    }
  }

  if (gen) {
    generate(m, n, l);
  } else {
    // read instance on standard input
    std::cin >> m >> n;
    int** capacities = new int*[m];
    for (int i = 0; i < m; ++i) {
      capacities[i] = new int[n];
      for (int j = 0; j < n; ++j) {
        std::cin >> capacities[i][j];
      }
    }

    solve(capacities, m, n, find_all);
  }

  return EXIT_SUCCESS;
}